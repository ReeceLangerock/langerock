const mongoose = require('mongoose')
const Schema = mongoose.Schema

const blogSchema = new Schema(
	{
		author: String,
		date: Date,
		title: String,
		content: Object,


	}


)

const blogModel = mongoose.model('blog', blogSchema, 'blog')

module.exports = blogModel
