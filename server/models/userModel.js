const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
	username: String,
	firstname: String,
	lastname: String,
	password: String,
	isAdmin: Boolean,
	alias: String,
	email: String,
	codeUsed: Boolean
});

const userModel = mongoose.model('user', userSchema, 'user');

module.exports = userModel;
