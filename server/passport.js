var JWTStrategy = require('passport-jwt').Strategy;
var ExtractJWT = require('passport-jwt').ExtractJwt;
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const User = require('./models/userModel');
const bcrypt = require('bcrypt');

passport.use(
	new LocalStrategy(
		{
			usernameField: 'username',
			passwordField: 'password'
		},
		function(username, password, cb) {
			//this one is typically a DB call. Assume that the returned user object is pre-formatted and ready for storing in JWT

			return User.findOne({ username })
				.then(user => {
					bcrypt.compare(password, user.password, function(err, res) {
						if (res) {
							return cb(null, user, { message: 'Logged In Successfully' });
						} else {
							console.log('hash fail', password, user.password);
							return cb(null, false, { message: 'Incorrect username or password.' });
						}
					});
					if (!user) {
						return cb(null, false, { message: 'Incorrect username or password.' });
					}
				})
				.catch(err => console.log(err));
		}
	)
);

passport.use(
	new JWTStrategy(
		{
			jwtFromRequest: ExtractJWT.fromAuthHeaderAsBearerToken(),
			secretOrKey: 'your_jwt_secret'
		},
		function(jwtPayload, cb) {
			//find the user in db if needed. This functionality may be omitted if you store everything you'll need in JWT payload.
			return UserModel.findOneById(jwtPayload.id)
				.then(user => {
					return cb(null, user);
				})
				.catch(err => {
					return cb(err);
				});
		}
	)
);
