const express = require('express');
const router = express.Router();
const request = require('request-promise');
const Youtube = require('../models/youtubeModel');

getNewAccessToken = async function() {
	const refresh_token = process.env.REFRESH_TOKEN;
	const client_id = process.env.CLIENT_ID;
	const client_secret = process.env.CLIENT_SECRET;

	try {
		let result = await request.post('https://www.googleapis.com/oauth2/v4/token', {
			headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
			options: {
				json: true
			},
			formData: {
				refresh_token: refresh_token,
				grant_type: 'refresh_token',
				client_secret: client_secret,
				client_id: client_id
			}
		});

		result = JSON.parse(result);
		return result.access_token;
	} catch (err) {
		console.log('err', err);
	}
};

router.get('/photos', async function(req, res, next) {
	let albums = [];
	let error = null;
	let nextPageToken = req.query.nextPageToken;
	let parameters = {
		pageSize: '50',
		albumId: 'ALAWPZYf9_rWR08gUS8GDFl4ZTWJY1aTuOqDtwc3uxMjHm8seHAqoQGM_Th_HatO3-0dY1J263UG'
	};

	if (nextPageToken !== 'no_token') {
		console.log('addingToken');
		parameters.pageToken = nextPageToken;
	}

	try {
		// Loop while there is a nextpageToken property in the response until all
		// albums have been listed.
		do {
			// logger.verbose(`Loading albums. Received so far: ${albums.length}`);
			// Make a GET request to load the albums with optional parameters (the
			// pageToken if set).
			const access_token = await getNewAccessToken();

			const result = await request.post(
				'https://photoslibrary.googleapis.com/v1/mediaItems:search',
				{
					headers: { 'Content-Type': 'application/json' },
					json: true,
					auth: {
						bearer: access_token
					},
					body: parameters
				}
			);

			// console.dir(result, { depth: null });
			if (result && result.albums) {
				//   logger.verbose(`Number of albums received: ${result.albums.length}`);
				// Parse albums and add them to the list, skipping empty entries.
				const items = result.albums.filter(x => !!x);

				albums = albums.concat(items);
				console.log(result);
			}
			parameters.pageToken = result.nextPageToken;
			// Loop until all albums have been listed and no new nextPageToken is
			// returned.
			res.json({ albums: result });
		} while (parameters.pageToken != null);
	} catch (err) {
		console.log('err', err.message);
		// If the error is a StatusCodeError, it contains an error.error object that
		// should be returned. It has a name, statuscode and message in the correct
		// format. Otherwise extract the properties.
		error = err.error.error || { name: err.name, code: err.statusCode, message: err.message };
		//   logger.error(error);
	}

	//THIS NEEDS TO bE FIGURED PUT AND CLEANED UP STILL
	//photoos api
	/*
      const mediaItemCache = persist.create({
        dir: "persist-mediaitemcache/",
        ttl: 3300000 // 55 minutes
      });
      mediaItemCache.init();
  
      const albumCache = persist.create({
        dir: "persist-albumcache/",
        ttl: 600000 // 10 minutes
      });
      albumCache.init();
  
      const storage = persist.create({ dir: "persist-storage/" });
      storage.init();
  
      // Set up a session middleware to handle user sessions.
      // NOTE: A secret is used to sign the cookie. This is just used for this sample
      // app and should be changed.
      const sessionMiddleware = session({
        resave: true,
        saveUninitialized: true,
        store: new fileStore({}),
        secret: "photo frame sample"
      });
  
      // Enable user session handling.
      server.use(sessionMiddleware);
      */

	/*
// Returns all albums owned by the user.
server.get("/getAlbums", async (req, res) => {
  logger.info("Loading albums");
  const userId = req.user.profile.id;

  // Attempt to load the albums from cache if available.
  // Temporarily caching the albums makes the app more responsive.
  const cachedAlbums = await albumCache.getItem(userId);
  if (cachedAlbums) {
    logger.verbose("Loaded albums from cache.");
    res.status(200).send(cachedAlbums);
  } else {
    logger.verbose("Loading albums from API.");
    // Albums not in cache, retrieve the albums from the Library API
    // and return them
    const data = await libraryApiGetAlbums(req.user.token);
    if (data.error) {
      // Error occured during the request. Albums could not be loaded.
      returnError(res, data);
      // Clear the cached albums.
      albumCache.removeItem(userId);
    } else {
      // Albums were successfully loaded from the API. Cache them
      // temporarily to speed up the next request and return them.
      // The cache implementation automatically clears the data when the TTL is
      // reached.
      res.status(200).send(data);
      albumCache.setItemSync(userId, data);
    }
  }
});*/
});

router.get('/youtube', function(req, res, next) {
	Youtube.find({})
		.limit(10)
		.then(videos => {
			console.log('found posts: ', videos);
			res.status(200).json(videos);
		})
		.catch(err => console.log(err));
});

module.exports = router;
