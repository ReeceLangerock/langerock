const express = require("express");
const router = express.Router();

const Youtube = require("../models/youtubeModel");


router.post("/youtube", async function (req, res, next) {

    const { link, title } = req.body

    if (link.length === 0 || link.title === 0) {
        res.status(422).json({ error: "Submission is invalid" })
        res.end()
        return

    }
    var expression = /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi;
    var regex = new RegExp(expression);
    if (!link.match(regex)) {
        res.status(422).json({ error: "Submitted Link is invalid" })
        res.end()
        return

    }
    const videoExists = await Youtube.findOne({
        $or: [{ link: link }, { title: title }]
    })

    if (!videoExists) {
        const newVideo = await new Youtube({ link: link, title, title })
        await newVideo.save()
        res.status(200).json({ success: true })
        res.end()
        return

    } else {
        res.status(409).json({ error: "Video already submitted" })
        res.end()
        return

    }


});

module.exports = router;
