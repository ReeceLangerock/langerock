const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const passport = require('passport');
const bcrypt = require('bcrypt');

const User = require('./../models/userModel');

router.post('/signin', function(req, res, next) {
	passport.authenticate('local', { session: false }, (err, user, info) => {
		if (err) {
			return res.status(400).json({
				error: 'Something is not right'
			});
		}
		if (!user) {
			return res.status(400).json({
				error: 'Incorrect username or password.'
			});
		}
		req.login(user, { session: false }, err => {
			if (err) {
				res.send(err);
			}
			// generate a signed son web token with the contents of user object and return it in the response
			const token = jwt.sign(user.toJSON(), process.env.JWT_SECRET, { expiresIn: '14d' });
			const tokenUser = {
				username: user.username,
				firstname: user.firstname,
				lastname: user.lastname,
				email: user.email,
				alias: user.alias,
				token: token,
				isAdmin: user.isAdmin,
				_id: user._id
			};

			return res.json({ tokenUser, token });
		});
	})(req, res);
});

router.post('/reauth', async function(req, res, next) {
	const { _id, token } = req.body;
	let legit = false;
	try {
		legit = jwt.verify(token, 'your_jwt_secret', { expiresIn: '7d' });
	} catch (e) {}
	if (legit) {
		const UserFound = await User.findOne({ _id });

		if (UserFound) {
			const token = jwt.sign(UserFound.toJSON(), 'your_jwt_secret', { expiresIn: '14d' });
			const tokenUser = {
				username: UserFound.username,
				firstname: UserFound.firstname,
				lastname: UserFound.lastname,
				email: UserFound.email,
				alias: UserFound.alias,
				token: token,
				isAdmin: UserFound.isAdmin,
				_id: UserFound._id
			};

			return res.json({ tokenUser, token });
		} else {
			res.status(400).json({ error: 'Unable to sign you back in' });
			return;
		}
	} else {
		res.status(400).json({ error: 'Unable to sign you back in' });
		return;
	}
});

router.post('/signup', async function(req, res, next) {
	const { accessCode, username, password, passwordRetype, email } = req.body;

	if (password !== passwordRetype) {
		res.status(400).json({ error: 'Passwords do not match' });
		return;
	}

	if (password.length < 6 || passwordRetype.length < 6 || username.length < 3) {
		res.status(400).json({ error: 'Username or Password does not meet requirements' });
		return;
	}

	const UserFound = await User.findOne({ accessCode });

	if (UserFound) {
		if (UserFound.codeUsed) {
			res.status(400).json({ error: 'This Access Code has already been used, please login.' });
			return;
		} else {
			let hash = bcrypt.hashSync(password, 10);
			UserFound.password = hash;
			UserFound.username = username;
			UserFound.email = email;
			UserFound.codeUsed = true;

			await UserFound.save();
		}
	}
	if (!UserFound) {
		res.status(400).json({ error: 'Access Code is invalid, please check again' });
		return;
	}

	passport.authenticate('local', { session: false }, (err, user, info) => {
		if (err) {
			return res.status(400).json({
				message: 'Something is not right'
			});
		}

		req.login(user, { session: false }, err => {
			if (err) {
				res.send(err);
			}
			const token = jwt.sign(user.toJSON(), 'your_jwt_secret');
			const tokenUser = {
				username: user.username,
				firstname: user.firstname,
				lastname: user.lastname,
				email: user.email,
				alias: user.alias,
				token: token,
				isAdmin: user.isAdmin
			};
			return res.json({ tokenUser, token });
		});
	})(req, res);
});

module.exports = router;
