const express = require("express");
const router = express.Router();

const Blog = require("./../models/blogModel");

router.get("/", function(req, res, next) {
  Blog.find({})
    .limit(10)
    .then(posts => {
      res.status(200).json(posts);
    })
    .catch(err => console.log(err));
});

router.post("/", async function(req, res, next) {

    const {title, user, content} = req.body 

    if (title.length < 5 || content.length < 20) {
        res.status(400),json({error: 'Blog post must include content'})
    }
    
    const author = user.alias ? user.alias : `${user.firstname} ${user.lastname}`
    const newPost = await new Blog({
        title, author, content, date: new Date()
    })
    
    console.log(newPost)
    newPost.save()


    res.status(200).json({success: 'Blog post added'})
    res.end()
    return;
});

module.exports = router;
