('use strict');
const nodemailer = require('nodemailer');
const express = require('express');
const router = express.Router();

router.post('/', function(req, res, next) {
	const { user, title, message } = req.body;

	if (title.length === 0 || message.length === 0) {
		res.status(400).json({ error: 'There must be content in the email' });
		res.end();
		return;
	}
	// Generate test SMTP service account from ethereal.email
	// Only needed if you don't have a real mail account for testing
	nodemailer.createTestAccount((err, account) => {
		// create reusable transporter object using the default SMTP transport
		console.log(process.env.GMAIL_USER);
		let transporter = nodemailer.createTransport({
			// host: 'smtp.ethereal.email',
			// port: 587,
			// secure: false, // true for 465, false for other ports
			service: 'gmail',
			auth: {
				user: process.env.GMAIL_USER, // generated ethereal user
				pass: process.env.GMAIL_PASSWORD // generated ethereal password
			}
		});
		const userName = user.alias ? user.alias : `${user.firstname} ${user.lastname}`;
		const userEmail = user.email ? user.email : `mclangerocks@gmail.com`;
		// setup email data with unicode symbols
		let mailOptions = {
			from: `"${userName}" <${userEmail}>`, // sender address
			to: 'lilylangerock@gmail.com', // list of receivers
			subject: title, // Subject line
			text: message // plain text body
			// html: '<b>Hello world?</b>' // html body
		};

		// send mail with defined transport object
		transporter.sendMail(mailOptions, (error, info) => {
			if (error) {
				console.log(error);
				res.status(400).json({ success: 'There was an error sending the message!' });
				res.end();
				return;
			} else {
				res.status(200).json({ success: 'Message successfully sent to Lily!' });
				res.end();
				return;
			}
		});
	});
});
module.exports = router;
