require('dotenv').config();
const express = require('express');
const next = require('next');
const api_database = require('./api/database.js');
const api_media = require('./api/media.js');
// const api_drive = require('./api/drive.js');
const api_blog = require('./api/blog.js');
const api_email = require('./api/nodemailer.js');
const api_admin = require('./api/admin.js');
const body = require('body-parser');
const dev = process.env.NODE_ENV === 'DEV';
const app = next({ dev });
const handle = app.getRequestHandler();
const mongoose = require('./config/mongoose');
require('./passport');
console.log(process.env.PORT);
app
	.prepare()
	.then(async () => {
		const server = express();

		server.use(body.json());

		// open mongoose connection
		mongoose.connect();

		// Console transport for winton.

		// Enable extensive logging if the DEBUG environment variable is set.
		if (process.env.DEBUG) {
			// Print all winston log levels.

			// Enable request debugging.
			require('request-promise').debug = true;
		} else {
			// By default, only print all 'verbose' log level messages or below.
		}

		// // Set up passport and session handling.
		// server.use(passport.initialize());
		// server.use(passport.session());

		// Middleware that adds the user of this session as a local variable,
		// so it can be displayed on all pages when logged in.
		server.use((req, res, next) => {
			res.locals.name = '-';
			if (req.user && req.user.profile && req.user.profile.name) {
				res.locals.name = req.user.profile.name.givenName || req.user.profile.displayName;
			}

			res.locals.avatarUrl = '';
			if (req.user && req.user.profile && req.user.profile.photos) {
				res.locals.avatarUrl = req.user.profile.photos[0].value;
			}
			next();
		});

		server.use('/api/auth', api_database);
		server.use('/api/media', api_media);
		// server.use('/api/drive', api_drive);
		server.use('/api/blog', api_blog);
		server.use('/api/admin', api_admin);
		server.use('/api/email', api_email);

		server.get('*', (req, res) => {
			return handle(req, res);
		});

		server.listen(process.env.PORT, err => {
			if (err) throw err;
			console.log('> NextJS server running on http://localhost:3000');
		});
	})
	.catch(ex => {
		console.error(ex.stack);
		process.exit(1);
	});

/**
 * Express instance
 * @public
 */
module.exports = app;
