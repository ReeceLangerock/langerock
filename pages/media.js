import React from "react";
import { connect } from "react-redux";
import Layout from "./../src/components/Layout";
import PhotoTab from "./../src/components/media_page/PhotoTab";
import YouTube from "./../src/components/media_page/YouTube";
import LandingPage from "./../src/components/LandingPage";

import { Tab } from "semantic-ui-react";


class Media extends React.Component {


  render() {
    const { authenticated } = this.props;
    if (authenticated) {
      const panes = [
        {
          menuItem: "Photos",
          render: () => (
            <Tab.Pane attached={false} style={{ textAlign: 'right', border: 'none', boxShadow: 'none', padding: 0, margin: '0 5px' }} >
              <PhotoTab />
            </Tab.Pane >
          )
        },
        {
          menuItem: "YouTube", render: () => <Tab.Pane attached={false} style={{ border: 'none', boxShadow: 'none', padding: 0 }}>

            <YouTube />
          </Tab.Pane>
        },
        { menuItem: "Social Media", render: () => <Tab.Pane attached={false} >Nothing to see here yet!</Tab.Pane> }
      ];
      // const TabExampleSecondaryPointing = () => ()

      return (
        <Layout>
          <Tab menu={{ secondary: true, pointing: true }} panes={panes} />
        </Layout>
      );
    } else {
      return <LandingPage />;

    }
  }
}
const mapStateToProps = state => ({
  authenticated: state.authenticated
})
export default connect(mapStateToProps)(Media);

