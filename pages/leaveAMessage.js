import React from 'react';
import { connect } from 'react-redux';
import Layout from '../src/components/Layout';
import styled from 'styled-components';
import fetch from 'isomorphic-unfetch';
import { Button, TextArea, Form, Segment, Input, Divider } from 'semantic-ui-react';
import LandingPage from '../src/components/LandingPage';
import Message from './../src/components/Message';

class LeaveAMessage extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			title: '',
			message: '',
			res: ''
		};
		this.sendMessage = this.sendMessage.bind(this);
	}

	async sendMessage() {
		const { title, message } = this.state;

		fetch(`${this.props.routePrefix}/email`, {
			method: 'POST', // *GET, POST, PUT, DELETE, etc.
			headers: {
				'Content-Type': 'application/json; charset=utf-8'
			},
			body: JSON.stringify({ user: this.props.user, title: title, message: message }) // body data type must match "Content-Type" header
		})
			.then(response => response.json())
			.then(data => {
				if (data.success) {
					this.setState({
						title: '',
						message: ''
					});
				}
				this.setState({ res: data });
			})
			.then(() => {
				setTimeout(() => {
					this.setState({ res: undefined });
				}, 3000);
			});
	}
	render() {
		const { message, title } = this.state;
		const { authenticated, user } = this.props;

		if (authenticated) {
			return (
				<Layout>
					<MessageContainer>
						<h2>Send Lily a Message!</h2>
						<p>
							It doesn't matter what, a funny story, a heartfelt message, maybe just a quick hello.
							Leave your message here and it will be emailed to Lily's gmail account and stored in a
							"{user.firstname}" folder with any other message you've left her. It might take her a
							few years to get back to you though.
						</p>
						<Message data={this.state.res} />
						<Divider />
						<Form>
							<Form.Field
								control={Input}
								label="Title"
								required
								placeholder="Title"
								onChange={e => this.setState({ title: e.target.value })}
								value={title}
							/>

							<Form.Field
								control={TextArea}
								label="Message"
								required
								onChange={e => this.setState({ message: e.target.value })}
								value={message}
								placeholder="Leave your message for Lily here..."
							/>

							<Button type="submit" onClick={() => this.sendMessage()}>
								Submit
							</Button>
						</Form>
					</MessageContainer>
				</Layout>
			);
		} else {
			return <LandingPage />;
		}
	}
}

const MessageContainer = styled(Segment)`
	margin-top: 20px !important;
`;
const mapStateToProps = state => ({
	user: state.user,
	authenticated: state.authenticated,
	routePrefix: state.routePrefix
});
export default connect(
	mapStateToProps,
	null
)(LeaveAMessage);
