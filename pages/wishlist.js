import React from "react";
import { connect } from "react-redux";
import Layout from "./../src/components/Layout";
import Wishlist from "./../src/components/Wishlist";
import LandingPage from "./../src/components/LandingPage";

class WishlistPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      authenticated: false
    };
  }


  render() {
    const { authenticated } = this.props;

    if (authenticated) {
      return (
        <Layout>
          <Wishlist />
        </Layout>
      );
    } else {
      return <LandingPage />;
    }
  }
}

const mapStateToProps = state => ({
  authenticated: state.authenticated
})

export default connect(mapStateToProps, null)(WishlistPage);
