import React from "react";
import { connect } from "react-redux";
import Layout from "./../src/components/Layout";
import Admin from "./../src/components/Admin";
import LandingPage from "./../src/components/LandingPage";

class AdminPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      authenticated: false
    };
  }

  render() {
    const { authenticated } = this.props;
    if (authenticated) {
      return (
        <Layout>
          <Admin />
        </Layout>
      );
    } else {
      return <LandingPage />;
    }
  }
}

const mapStateToProps = state => ({
  authenticated: state.authenticated
})

export default connect(mapStateToProps, null)(AdminPage);
