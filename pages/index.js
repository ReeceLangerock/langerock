import React from "react";
import { connect } from "react-redux";
import Blog from "./../src/components/Blog";
import LandingPage from "./../src/components/LandingPage";

class Index extends React.Component {

  render() {
    const { authenticated } = this.props;
    if (authenticated) {
      return <Blog />;
    } else {
      return <LandingPage />;
    }
  }
}

const mapStateToProps = state => ({
  authenticated: state.authenticated
})

export default connect(mapStateToProps, null)(Index);
