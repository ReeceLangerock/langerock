import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunkMiddleware from 'redux-thunk';

const exampleInitialState = {
	authenticated: false, // set this to false for prod
	newUser: false,
	user: {},
	blogPosts: [],
	isDev: false,
	routePrefix: 'http://langerock.herokuapp.com/api'
};

// REDUCERS
export const reducer = (state = exampleInitialState, action) => {
	switch (action.type) {
		case 'USER_LOGGED_IN':
			return {
				...state,
				authenticated: true,
				user: action.user
			};
		case 'STORE_BLOG_POSTS':
			return {
				...state,
				blogPosts: action.blogPosts
			};
		case 'TOGGLE_USER_TYPE':
			return {
				...state,
				newUser: !state.newUser
			};
		case 'SIGN_OUT':
			return {
				...state,
				authenticated: false
			};
		case 'SET_IS_DEV':
			return {
				...state,
				isDev: action.isDev,
				routePrefix: action.routePrefix
			};

		default:
			return state;
	}
};

export function initializeStore(initialState = exampleInitialState) {
	return createStore(reducer, initialState, composeWithDevTools(applyMiddleware(thunkMiddleware)));
}
