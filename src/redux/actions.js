export const handleUserLogin = user => dispatch => {
	storeWebToken();
	return dispatch({ type: 'USER_LOGGED_IN', user });
};

export const toggleUserType = () => dispatch => {
	return dispatch({ type: 'TOGGLE_USER_TYPE' });
};
export const storeBlogPosts = blogPosts => dispatch => {
	return dispatch({ type: 'STORE_BLOG_POSTS', blogPosts });
};

export const signOut = () => dispatch => {
	localStorage.removeItem('user');
	return dispatch({ type: 'SIGN_OUT' });
};

export const setIsDev = (isDev, routePrefix) => dispatch => {
	return dispatch({ type: 'SET_IS_DEV', isDev, routePrefix });
};

export const storeWebToken = user => dispatch => {
	localStorage.setItem('user', user);
};
