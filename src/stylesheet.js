var stylesheet = {
    white: "#fff",
    red: "#E05263",
    grey: "#D2D9DB",
    green: "#3DDC97",
    blue: "#5BC3EB",
    red: "#E05263",
    darkGrey: "#3A3C3C",

    segment: `
    box-shadow: 0 1px 2px 0 rgba(34,36,38,.15);
    margin: 1rem 0;
    padding: 1em 1em;
    border-radius: .28571429rem;
    border: 1px solid rgba(34,36,38,.15);
    `
};

stylesheet.functions = {
    boxShadow: (hex, opacity, size = "0 2px 3px 0") => {
        var c;
        var rgba = ''

        if (/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)) {
            c = hex.substring(1).split("");
            if (c.length == 3) {
                c = [c[0], c[0], c[1], c[1], c[2], c[2]];
            }
            c = "0x" + c.join("");
            rgba = "rgba(" + [(c >> 16) & 255, (c >> 8) & 255, c & 255].join(",") + "," + opacity + ")";
        }
        return `
        box-shadow: ${size} ${rgba};
        `;
    }
};
export default stylesheet;
