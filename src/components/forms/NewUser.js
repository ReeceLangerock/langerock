import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { Button, Input, Form } from 'semantic-ui-react';
import stylesheet from './../../stylesheet';
import Message from './../Message';
import * as actions from './../../redux/actions';

class NewUser extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			newUser: {
				accessCode: '',
				username: '',
				password: '',
				passwordRetype: '',
				email: ''
			},
			message: undefined
		};
		this.signUp = this.signUp.bind(this);
		this.preFlightCheck = this.preFlightCheck.bind(this);
	}

	preFlightCheck() {
		const { newUser } = this.state;
		const message = { error: '' };

		if (newUser.password.length < 6 || newUser.passwordRetype.length < 6) {
			message.error = 'Password must be at least 6 characters!';
		}
		if (newUser.password !== newUser.passwordRetype) {
			message.error = "Passwords don't match!";
		}

		var emailRegex = new RegExp(
			/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
		);
		if (newUser.email.match(emailRegex) === null) {
			message.error = 'Email format is invalid';
		}
		if (newUser.username.length < 3) {
			message.error = 'Username must be at least 3 characters!';
		}
		if (newUser.accessCode.length !== 6) {
			message.error = 'Access code should be 6 characters';
		}

		if (message.error) {
			this.setState({
				message
			});
			return false;
		}
	}

	async signUp() {
		if (this.preFlightCheck() === false) {
			setTimeout(() => {
				this.setState({ message: undefined });
			}, 4000);
			return;
		}
		const { newUser } = this.state;
		const signinResponse = await fetch(`${this.props.routePrefix}/auth/signup`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json; charset=utf-8'
			},
			body: JSON.stringify({
				username: newUser.username,
				password: newUser.password,
				passwordRetype: newUser.passwordRetype,
				accessCode: newUser.accessCode,
				email: newUser.email
			})
		})
			.then(response => response.json())
			.then(data => {
				console.log(data);
				if (data.token) {
					this.props.handleUserLogin(data.tokenUser);
					this.props.storeWebToken(JSON.stringify(data.tokenUser));
				} else {
					this.setState({ message: data });
					setTimeout(() => {
						this.setState({ message: undefined });
					}, 3000);
				}
			});
	}

	render() {
		return (
			<AuthContainer>
				<h1>Hi there! Please sign up</h1>
				<p>Don't have an access code? Contact Reece or Shelly to get one!</p>
				<Message data={this.state.message} />

				<Form>
					<Form.Field
						placeholder="Access Code"
						label="Access Code"
						required
						control={Input}
						onChange={e =>
							this.setState({
								newUser: { ...this.state.newUser, accessCode: e.target.value }
							})
						}
					/>
					<Form.Field
						placeholder="User Name"
						label="User Name"
						required
						control={Input}
						onChange={e =>
							this.setState({
								newUser: { ...this.state.newUser, username: e.target.value }
							})
						}
					/>
					<Form.Field
						control={Input}
						label="Email Address"
						placeholder="Email Address"
						required
						onChange={e =>
							this.setState({
								newUser: { ...this.state.newUser, email: e.target.value }
							})
						}
					/>

					<Form.Field
						type="password"
						control={Input}
						label="Password"
						placeholder="Password"
						required
						onChange={e =>
							this.setState({
								newUser: { ...this.state.newUser, password: e.target.value }
							})
						}
					/>
					<Form.Field
						control={Input}
						type="password"
						required
						placeholder="Reenter Password"
						label="Reenter Password"
						onChange={e =>
							this.setState({
								newUser: { ...this.state.newUser, passwordRetype: e.target.value }
							})
						}
					/>

					<ActionContainer>
						<Button
							style={{ flex: 1, background: stylesheet.green }}
							type="submit"
							onClick={this.signUp}
						>
							Submit
						</Button>
						<ToggleForm
							onClick={() => {
								this.props.toggleUserType();
							}}
						>
							Already signed up? Click here!
						</ToggleForm>
					</ActionContainer>
				</Form>
			</AuthContainer>
		);
	}
}

const mapStateToProps = state => ({
	authenticated: state.authenticated,
	routePrefix: state.routePrefix
});

export default connect(
	mapStateToProps,
	actions
)(NewUser);

const AuthContainer = styled.div`
	background: ${stylesheet.white};
	max-width: 600px;
	min-width: 300px;
	align-self: center;
	position: relative;
	box-shadow: 0 1px 2px 0 rgba(34, 36, 38, 0.15);
	margin: 1rem 0;
	padding: 1em 1em;
	border-radius: 0.28571429rem;
	border: 1px solid rgba(34, 36, 38, 0.15);
	transition: 1s ease-in-out all;
	h1,
	p {
		margin-bottom: 8px;
	}
	:hover {
		box-shadow: 0 2px 3px 0 rgba(34, 36, 38, 0.55);
		transition: 0.5s ease-in-out all;
	}
	@media (max-width: 600px) {
		padding: 0.5em;
		margin: 5px;
	}
`;
const ActionContainer = styled.div`
	display: flex;
	justify-content: space-between;
	align-items: center;
`;

const ToggleForm = styled.div`
	cursor: pointer;
	margin-left: 10px;
`;
