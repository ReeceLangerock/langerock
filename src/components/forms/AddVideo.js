import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { Button, Form } from 'semantic-ui-react';
import stylesheet from './../../stylesheet';
import Message from './../Message';
import * as actions from './../../redux/actions';

class Index extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			title: '',
			link: '',
			message: undefined
		};
		this.addVideo = this.addVideo.bind(this);
	}

	async addVideo() {
		const { title, link } = this.state;

		const addVideoResponse = await fetch(`${this.props.routePrefix}/admin/youtube/`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json; charset=utf-8'
			},
			body: JSON.stringify({ title, link })
		})
			.then(response => response.json())
			.then(data => {
				this.setState({ message: data });
			})
			.then(() => {
				setTimeout(() => {
					this.setState({ message: undefined });
				}, 3000);
			});
	}

	render() {
		return (
			<div>
				<Header>Add A YouTube Video</Header>
				<Message data={this.state.message} />
				<P>Make sure you have the correct url before submitting</P>
				<Form>
					<Form.Field>
						<label>Video Title</label>

						<input placeholder="Title" onChange={e => this.setState({ title: e.target.value })} />
					</Form.Field>
					<Form.Field>
						<label>Video URL</label>
						<input
							type="text"
							placeholder="Video URL"
							onChange={e => this.setState({ link: e.target.value })}
						/>
					</Form.Field>
					<ActionContainer>
						<Button style={{ background: stylesheet.green }} type="submit" onClick={this.addVideo}>
							Add Video!
						</Button>
					</ActionContainer>
				</Form>
			</div>
		);
	}
}

const mapStateToProps = state => ({
	authenticated: state.authenticated,
	routePrefix: state.routePrefix
});

export default connect(
	mapStateToProps,
	actions
)(Index);

const Header = styled.div`
	color: ${stylesheet.darkGrey};
	font-size: 20px;
`;
const P = styled.p`
	color: ${stylesheet.darkGrey};
	font-size: 14px;
	margin-top: 0;
	font-weight: normal;
`;

const ActionContainer = styled.div`
	display: flex;
	justify-content: space-between;
	align-items: center;
`;
