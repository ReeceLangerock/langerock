import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { Button, Form } from 'semantic-ui-react';
import stylesheet from './../../stylesheet';
import Message from './../Message';
import * as actions from './../../redux/actions';
import { Editor } from 'react-draft-wysiwyg';
import { EditorState, convertToRaw } from 'draft-js';
import draftToHtml from 'draftjs-to-html';

import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';

class Index extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			title: '',
			editorState: EditorState.createEmpty()
		};
		this.onEditorStateChange = this.onEditorStateChange.bind(this);
		this.addBlogPost = this.addBlogPost.bind(this);
	}

	onEditorStateChange(editorState) {
		this.setState({
			editorState
		});
	}

	async addBlogPost() {
		const { title, editorState } = this.state;
		const { user, routePrefix } = this.props;

		const content = draftToHtml(convertToRaw(editorState.getCurrentContent()));
		if (title.length >= 5 || content.length >= 20) {
			fetch(`${routePrefix}/blog`, {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json; charset=utf-8'
				},
				body: JSON.stringify({ title, content, user })
			})
				.then(response => response.json())
				.then(data => {
					if (data.success) {
						console.log(this.state);
						this.setState(
							{
								editorState: EditorState.createEmpty(),
								title: '',
								message: data
							},
							console.log(this.state)
						);
					}
				})
				.then(() => {
					setTimeout(() => {
						this.setState({ message: undefined });
					}, 3000);
				});
		} else {
			this.setState({ message: { error: 'Blog post must include content' } });
			setTimeout(() => {
				this.setState({ message: undefined });
			}, 3000);
		}
	}

	render() {
		return (
			<div>
				<Header>Add A Blog Post</Header>
				<Message data={this.state.message} />
				<Form>
					<Form.Field>
						<label>Post Title</label>

						<input
							placeholder="Title"
							value={this.state.title}
							onChange={e => this.setState({ title: e.target.value })}
						/>
					</Form.Field>
					<Editor
						editorState={this.state.editorState}
						toolbarClassName="toolbarClassName"
						wrapperClassName="wrapperClassName"
						editorClassName="editorClassName"
						placeholder="What do you want to share?"
						editorStyle={{
							border: '1px solid #F1F1F1',
							maxHeight: '400px',
							borderRadius: '2px',
							padding: '0 8px',
							minHeight: '100px',
							margin: '15px 0'
						}}
						onEditorStateChange={this.onEditorStateChange}
					/>
					<ActionContainer>
						<Button
							style={{ background: stylesheet.green }}
							type="submit"
							onClick={this.addBlogPost}
						>
							Add Post!
						</Button>
					</ActionContainer>
				</Form>
			</div>
		);
	}
}

const mapStateToProps = state => ({
	user: state.user,
	routePrefix: state.routePrefix
});

export default connect(
	mapStateToProps,
	actions
)(Index);

const Header = styled.div`
	color: ${stylesheet.darkGrey};
	font-size: 20px;
	margin-bottom: 10px;
`;
const P = styled.p`
	color: ${stylesheet.darkGrey};
	font-size: 14px;
	margin-top: 0;
	font-weight: normal;
`;

const ActionContainer = styled.div`
	display: flex;
	justify-content: space-between;
	align-items: center;
`;
