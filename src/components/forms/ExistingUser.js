import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { Button, Form } from 'semantic-ui-react';
import stylesheet from './../../stylesheet';
import * as actions from './../../redux/actions';
import Message from './../Message';

class Index extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			existingUser: {
				username: '',
				password: ''
			},
			message: undefined
		};
		this.signIn = this.signIn.bind(this);
	}

	async signIn() {
		const { existingUser } = this.state;
		const signinResponse = await fetch(`${this.props.routePrefix}/auth/signin`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json; charset=utf-8'
			},
			body: JSON.stringify({ username: existingUser.username, password: existingUser.password })
		})
			.then(response => response.json())
			.then(data => {
				console.log(data);
				if (data.token) {
					this.props.handleUserLogin(data.tokenUser);
					this.props.storeWebToken(JSON.stringify(data.tokenUser));
				} else {
					this.setState({ message: data });
					setTimeout(() => {
						this.setState({ message: undefined });
					}, 3000);
				}
			});
	}

	render() {
		return (
			<AuthContainer>
				<Header>Welcome, please sign in!</Header>
				<Message data={this.state.message} />
				<Form>
					<Form.Field>
						<label>User Name</label>
						<input
							placeholder="User Name"
							onChange={e =>
								this.setState({
									existingUser: { ...this.state.existingUser, username: e.target.value }
								})
							}
						/>
					</Form.Field>
					<Form.Field>
						<label>Password</label>
						<input
							type="password"
							placeholder="Password"
							onChange={e =>
								this.setState({
									existingUser: { ...this.state.existingUser, password: e.target.value }
								})
							}
						/>
					</Form.Field>
					<ActionContainer>
						<Button style={{ background: stylesheet.green }} type="submit" onClick={this.signIn}>
							Sign In
						</Button>
						<ToggleForm
							onClick={() => {
								this.props.toggleUserType();
							}}
						>
							Need to sign up still? Click here!
						</ToggleForm>
					</ActionContainer>
				</Form>
			</AuthContainer>
		);
	}
}

const mapStateToProps = state => ({
	authenticated: state.authenticated,
	routePrefix: state.routePrefix
});

export default connect(
	mapStateToProps,
	actions
)(Index);

const Header = styled.h1`
	color: ${stylesheet.darkGrey};
`;

const AuthContainer = styled.div`
	background: ${stylesheet.white};
	max-width: 600px;
	min-width: 300px;
	align-self: center;
	position: relative;
	box-shadow: 0 1px 2px 0 rgba(34, 36, 38, 0.15);
	margin: 1rem 0;
	padding: 1em 1em;
	border-radius: 0.28571429rem;
	border: 1px solid rgba(34, 36, 38, 0.15);
	transition: 1s ease-in-out all;

	:hover {
		box-shadow: 0 2px 3px 0 rgba(34, 36, 38, 0.55);
		transition: 0.5s ease-in-out all;
	}
	@media (max-width: 600px) {
		padding: 0.5em;
		margin: 5px;
	}
`;

const ActionContainer = styled.div`
	display: flex;
	justify-content: space-between;
	align-items: center;
`;

const ToggleForm = styled.div`
	cursor: pointer;
	margin-left: 10px;
`;
