import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import Moment from 'react-moment';
import Parser from 'html-react-parser';

import stylesheet from '../stylesheet';

class BlogEntry extends React.Component {
	render() {
		const { title, content, date, author } = this.props.entry;
		return (
			<EntryContainer>
				<Header>
					<h1>{title}</h1>
					<h3>
						Posted: <Moment format="MM/DD/YY">{date}</Moment>
					</h3>
				</Header>
				<Content>
					{Parser(content)}
					<AuthorSection>By: {author}</AuthorSection>
				</Content>
			</EntryContainer>
		);
	}
}

export default connect()(BlogEntry);

const EntryContainer = styled.div`
	${stylesheet.segment};
	margin: 0 5px 40px 5px;
	border-color: ${stylesheet.blue};
	transition: 0.5s ease-in-out all;
	padding: 0;
	:hover {
		transition: 0.5s ease-in-out all;
		${stylesheet.functions.boxShadow(stylesheet.blue, 0.6)};
	}
	img {
		max-width: 100%;
	}
`;

const Content = styled.div`
	padding: 1em 1em;
`;

const Header = styled.div`
	padding: 0.5em 1em;
	border-bottom: 1px solid ${stylesheet.grey};
	h1 {
		font-size: 26px;
		margin-bottom: 5px;
	}
	h3 {
		font-size: 18px;

		margin: 0;
	}
`;

const AuthorSection = styled.div`
	text-align: right;
	margin-top: 15px;
	font-weight: bold;
	font-size: 16px;
	color: ${stylesheet.blue};
`;
