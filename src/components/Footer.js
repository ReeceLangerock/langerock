import React, { Component } from "react";
import { Segment } from "semantic-ui-react";
import Moment from "react-moment";

import styled from "styled-components";

export default class Footer extends Component {
  constructor(props) {
    super(props)
    this.state = {
      date: ''
    }
  }
  componentDidMount() {
    this.setState({
      date: new Date()
    })

  }
  render() {
    return (
      <FooterContainer>
        <FooterText>
          &copy; The Langerocks - {this.state.date && <Moment format="YYYY">{this.state.date}</Moment>}
        </FooterText>
      </FooterContainer>
    );
  }
}

const FooterText = styled.div`
  text-align: center;
  color: grey;
  font-size: 13px;
`;
const FooterContainer = styled.div`
  background: white;
  grid-area: footer;
  max-width: 1000px;
  width: 100%;
  margin: auto auto 0 auto;
  background: #fff;
  box-shadow: 0 1px 2px 0 rgba(34, 36, 38, 0.15);
  padding: 0.3em 1em;
  border-radius: 0.28571429rem;
  border: 1px solid rgba(34, 36, 38, 0.15);
`;
