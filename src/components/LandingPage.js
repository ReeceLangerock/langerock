import { Segment, Button, Checkbox, Form } from 'semantic-ui-react';
import stylesheet from './../stylesheet';
import styled, { keyframes } from 'styled-components';

import { connect } from 'react-redux';
import * as actions from './../redux/actions';
import NewUser from './forms/NewUser';
import ExistingUser from './forms/ExistingUser';

class LandingPage extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			hidden: true,
			tokenFound: false
		};
	}

	componentWillMount() {
		this.setState({
			hidden: false
		});
	}
	async componentDidMount() {
		const isDev = window.location.hostname === 'localhost';
		const routePrefix = isDev ? 'http://localhost:3000/api' : 'http://langerock.herokuapp.com/api';
		this.props.setIsDev(isDev, routePrefix);

		var user = localStorage.getItem('user');
		user = user ? JSON.parse(user) : false;
		if (user && user.token) {
			this.setState({ tokenFound: true, userName: user.firstname });

			await fetch(`${routePrefix}/auth/reauth`, {
				method: 'POST',
				headers: {
					'Content-Type': 'application/json; charset=utf-8'
				},
				body: JSON.stringify({ _id: user._id, token: user.token })
			})
				.then(response => response.json())
				.then(data => {
					if (data.token) {
						this.props.handleUserLogin(data.tokenUser);
						this.props.storeWebToken(JSON.stringify(data.tokenUser));
					} else {
						this.setState({ message: data });
						localStorage.removeItem('user');
						this.setState({ tokenFound: false });
						setTimeout(() => {}, 3000);
					}
				});
		}
	}
	render() {
		const { newUser } = this.props;
		if (this.state.tokenFound) {
			return (
				<PageWrapper>
					<Title hidden={this.state.hidden}>Langerocks.ML</Title>
					<UserFound hidden={this.state.hidden}>
						Welcome Back {this.state.userName}!<p>Hang on a second, we're signing you in...</p>
					</UserFound>
				</PageWrapper>
			);
		}
		return (
			<PageWrapper>
				<Title hidden={this.state.hidden}>Langerocks.ML</Title>
				{newUser && <NewUser />}
				{!newUser && <ExistingUser />}
			</PageWrapper>
		);
	}
}
const mapStateToProps = state => ({
	newUser: state.newUser
});

export default connect(
	mapStateToProps,
	actions
)(LandingPage);

const Title = styled.h1`
	font-size: 84px;
	color: #3a3c3ccc;
	align-self: center;
	position: absolute;
	top: 11%;
	visibility: ${props => (props.hidden ? 'hidden' : 'visible')};
	animation: ${props => (props.hidden ? fadeOut : fadeIn)} 2s linear;
	transition: visibility 2s ease-in;
	@media (max-width: 600px) {
		font-size: 48px;
	}
`;

const PageWrapper = styled.div`
	background: url('https://images.unsplash.com/photo-1444837881208-4d46d5c1f127?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1501&q=80');
	background-repeat: no-repeat;
	background-size: cover;
	padding-top: 40px;
	height: 100vh;
	display: flex;
	justify-content: center;
	flex-direction: column;
`;

const UserFound = styled.div`
	text-align: center;
	color: #3a3c3ccc;
	font-size: 48px;
	font-weight: bold;
	visibility: ${props => (props.hidden ? 'hidden' : 'visible')};
	animation: ${props => (props.hidden ? fadeOut : fadeIn)} 2s linear;

	p {
		margin-top: 10px;
		font-size: 32px;
	}
`;

const fadeIn = keyframes`
  from {
    transform: scale(.9);
    opacity: 0;
  }

  to {
    transform: scale(1);
    opacity: 1;
  }
`;

const fadeOut = keyframes`
  from {
    transform: scale(1);
    opacity: 0;
  }

  to {
    transform: scale(.9);
    opacity: 1;
  }
`;
