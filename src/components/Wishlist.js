import React from "react";
import { connect } from "react-redux";
import styled from "styled-components";

import Layout from "./Layout";
import { Segment, Header } from "semantic-ui-react";

class Wishlist extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      authenticated: false
    };
  }

  render() {
    return (
      <Container>
        <Left>
          <Header as="h1" attached="top">
            Lily's Wishlist
            <SubHeader>Nobody likes duplicate presents! See what's on Lily's list</SubHeader>
          </Header>
          <Segment attached>
            <Header>
              Amazon -{" "}
              <a target="_blank" href="https://www.amazon.com/hz/wishlist/ls/32XRS08G8W1DT?&sort=default">
                Check it out!
              </a>
            </Header>
            <p />
          </Segment>
        </Left>
        <Right>
          <Header as="h2" attached="top">
            Lily's College Fund
            <SubHeader>Help send this girl to college!</SubHeader>
          </Header>

          <Segment attached>There's nothing to see here yet, I'm still researching options. Check back later!</Segment>
        </Right>
      </Container>
    );
  }
}

const Container = styled.div`
  /* display: flex; */
`;

const SubHeader = styled.p`
  font-size: 14px;
`;

const Left = styled.div`
  /* margin: 1rem 1rem 1rem 0; */
`;

const Right = styled.div`
  margin: 1rem 0;
`;

export default connect()(Wishlist);
