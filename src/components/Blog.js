import React from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import BlogPost from './BlogEntry';
import * as actions from './../redux/actions';

import Layout from './Layout';
import { parseTwoDigitYear } from 'moment';

class Blog extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			authenticated: false
		};
	}

	async retrieveBlogPosts() {
		const { existingUser } = this.state;
		const signinResponse = await fetch(`${this.props.routePrefix}/blog`, {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json; charset=utf-8'
			}
			// body: JSON.stringify({ pagination: 0 })
		})
			.then(response => response.json())
			.then(data => {
				console.log(data);
				this.props.storeBlogPosts(data.reverse());
			});
	}

	renderBlogEntries() {
		this.props.blogPosts;
		return this.props.blogPosts.map(post => {
			return <BlogPost entry={post} key={post._id} />;
		});
	}
	componentDidMount() {
		this.retrieveBlogPosts();
	}
	render() {
		return (
			<Layout>
				<h1>The Langerock Blog</h1>
				<BlogContainer>{this.renderBlogEntries()}</BlogContainer>
			</Layout>
		);
	}
}

const BlogContainer = styled.div``;
const mapStateToProps = state => ({
	blogPosts: state.blogPosts,
	routePrefix: state.routePrefix
});
export default connect(
	mapStateToProps,
	actions
)(Blog);
