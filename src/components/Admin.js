import React from "react";
import { connect } from "react-redux";
import styled from "styled-components";

import Layout from "./Layout";
import { Segment, Header } from "semantic-ui-react";
import AddVideo from "./forms/AddVideo";
import AddBlogPost from "./forms/AddBlogPost";

class Wishlist extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      authenticated: false
    };
  }

  render() {
    return (
      <Container>
        <Header attached="top">
          <AddBlogPost />
        </Header>
        
        <Header as="h1" attached="top">
          Reset a user password's
        </Header>
        <Segment attached>
          <p />
        </Segment>
        <Header as="h1" attached="top">
          Add A User
        </Header>
        <Header attached="top">
          <AddVideo />
        </Header>
        <Segment attached>
          <p />
        </Segment>
      </Container>
    );
  }
}

const Container = styled.div`
  /* display: flex; */
`;

const SubHeader = styled.p`
  font-size: 14px;
`;

const Left = styled.div`
  /* margin: 1rem 1rem 1rem 0; */
`;

const Right = styled.div`
  margin: 1rem 0;
`;

export default connect()(Wishlist);
