import React, { Component } from 'react';
import styled from 'styled-components';
import { Menu } from 'semantic-ui-react';
import Router from 'next/router';
import stylesheet from '../stylesheet';
import { connect } from 'react-redux';
import * as actions from './../redux/actions';

class Header extends Component {
	constructor(props) {
		super(props);
		this.state = { activeItem: 'home' };
		this.handleSignOut = this.handleSignOut.bind(this);
	}

	handleItemClick = (e, { name, value }) => {
		this.setState({ activeItem: value });
		Router.push({
			pathname: `/${value}`
		});
	};

	handleSignOut() {
		this.props.signOut();
	}

	render() {
		const { activeItem } = this.state;

		return (
			<StyledMenu size="small">
				<Center>
					<Item value="" name="home" active={activeItem === ''} onClick={this.handleItemClick} />
					<Item
						value="media"
						name="gallery"
						active={activeItem === 'media'}
						onClick={this.handleItemClick}
					/>
					<Item
						value="wishlist"
						name="Lily's Wishlist"
						active={activeItem === 'wishlist'}
						onClick={this.handleItemClick}
					/>
					<Item
						name="Leave Lily a Message"
						value="leaveAMessage"
						active={activeItem === 'leaveAMessage'}
						onClick={this.handleItemClick}
					/>

					<Menu.Menu position="right">
						<Item
							name="Sign Out"
							value="signout"
							active={activeItem === 'signout'}
							onClick={this.handleSignOut}
						/>
						{this.props.user.isAdmin && (
							<Item
								name="admin"
								value="admin"
								active={activeItem === 'admin'}
								onClick={this.handleItemClick}
							/>
						)}
					</Menu.Menu>
				</Center>
			</StyledMenu>
		);
	}
}

const mapStateToProps = state => ({
	user: state.user
});

export default connect(
	mapStateToProps,
	actions
)(Header);

const StyledMenu = styled(Menu)`
	grid-area: header;
	max-width: 1000px;
	width: 100%;
	text-align: left;
	margin: 0 auto !important;
`;

const Item = styled(Menu.Item)`
	cursor: pointer;
	color: ${props => (props.active ? stylesheet.blue : 'black')} !important;
	font-weight: ${props => (props.active ? 'bold' : 'normal')} !important;
`;

const Center = styled.div`
	display: inherit;
	width: 100%;
	max-width: 1000px;
`;
