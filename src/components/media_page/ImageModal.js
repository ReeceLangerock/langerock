import React, { Component } from "react";

import styled from "styled-components";
import Moment from "react-moment";
import stylesheet from '../../stylesheet.js'
export default class ImageModal extends Component {

  

  render() {
      if(this.props.image === undefined){
          return null
      }
    const { baseUrl, mediaMetadata, id } = this.props.image;
    return (
      <StyledCard  onClick = {this.displayFullSize} active = {this.props.image !== undefined}>
      <ImageFrame>


        <img src={baseUrl} />
      </ImageFrame>
      </StyledCard>
    );
  }
}

const ImageFrame = styled.div`
    background: white;
    border: 1px solid;

`

const StyledCard = styled.div`
    height: 90vh;
    width: 90vw;
    position: absolute;
    text-align: center;
    left: 0;
    right: 0;
    padding: 10px;
  display: ${props => props.active ? 'block' : ' none'};
`;
