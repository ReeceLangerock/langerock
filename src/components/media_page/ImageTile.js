import React, { Component } from "react";

import styled from "styled-components";
import Moment from "react-moment";
import stylesheet from '../../stylesheet.js'
export default class Footer extends Component {

  constructor(props){
    super(props)
    this.displayFullSize = this.displayFullSize.bind(this)
  }
  displayFullSize(){
    this.props.setModal(this.props.image)
  }

  render() {
    const { baseUrl, mediaMetadata, id } = this.props.image;
    return (
      <StyledCard size={this.props.size} key={id} onClick = {this.displayFullSize}>

      <Container>

        <img src={baseUrl} />

        <p>
          Added On: <Moment format="MM/DD/YY">{mediaMetadata.creationTime}</Moment>
        </p>
      </Container>
        
      </StyledCard>
    );
  }
}
const Container = styled.div`
  padding: 10px 5px;

 ${stylesheet.functions.boxShadow(stylesheet.blue, .4, "1px 1px 2px 2px")};

:hover {
  transition: .5s ease-in-out all;
${stylesheet.functions.boxShadow(stylesheet.blue, .7, "1px 1px 3px 3px")};

}
`

const StyledCard = styled.div`
  padding: 10px 5px;
  width: 100%;
  margin-top: 1em;
  margin-right: 5px;
  text-align: center;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  border-radius: 3px;
 

  img {
    max-width: ${props => props.size}
  }
  p{
    text-align: right;
    padding-right: 5px;
    padding-top: 5px;
    font-size: 16px;
    width: 100%;
  }
`;
