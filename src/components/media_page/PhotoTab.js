import React from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';
import fetch from 'isomorphic-unfetch';
import ImageTile from './ImageTile';
import ImageModal from './ImageModal';
import stylesheet from '../../stylesheet.js';
import { Loader } from 'semantic-ui-react';

class PhotoTab extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			tileSize: '200px',
			photos: 'loading',
			modaledImage: undefined,
			nextPageToken: 'no_token'
		};
		this.renderImages = this.renderImages.bind(this);
		this.setModaledImage = this.setModaledImage.bind(this);
		this.getPhotos = this.getPhotos.bind(this);
	}
	setModaledImage(image) {
		// add this later
		return;
		this.setState({
			modaledImage: image
		});
	}
	renderImages() {
		const images = this.state.photos;

		if (images.length === 0) {
			return (
				<Message>
					Failed to grab photos, please try again later or contact Reece if it is still not working
				</Message>
			);
		}
		if (images === 'loading') {
			return (
				<Message height="100px">
					<Loader active>Loading Photos</Loader>
				</Message>
			);
		}

		return images.map((image, index) => {
			return (
				<ImageTile
					image={image}
					size={this.state.tileSize}
					key={image.id + index}
					setModal={this.setModaledImage}
				/>
			);
		});
	}

	renderActionContainer() {
		const photosLoaded = this.state.photos.length;
		const tileSize = this.state.tileSize;
		if (photosLoaded) {
			return (
				<ActionContainer key="actionContainer">
					<Button
						active={tileSize === '90px'}
						key="button1"
						onClick={() => {
							this.setState({ tileSize: '90px' });
						}}
					>
						small
					</Button>
					<Button
						active={tileSize === '200px'}
						key="button2"
						onClick={() => {
							this.setState({ tileSize: '200px' });
						}}
					>
						medium
					</Button>
					<Button
						active={tileSize === '315px'}
						key="button3"
						onClick={() => {
							this.setState({ tileSize: '315px' });
						}}
					>
						large
					</Button>
				</ActionContainer>
			);
		}
	}

	async componentDidMount() {
		await this.getPhotos();
	}

	async getPhotos() {
		var photos = this.state.photos;
		const res = await fetch(
			`${this.props.routePrefix}/media/photos?&nextPageToken=${this.state.nextPageToken}`
		);
		const data = await res.json();
		if (photos !== 'loading') {
			photos = photos.concat(data.albums.mediaItems);
		} else {
			photos = data.albums.mediaItems;
		}
		this.setState({
			photos: photos,
			nextPageToken: data.albums.nextPageToken
		});
	}
	render() {
		return [
			<div>{this.renderActionContainer()}</div>,
			<ImageModal image={this.state.modaledImage} />,
			<ImageContainer tileSize={this.state.tileSize}>{this.renderImages()}</ImageContainer>,
			<ShowMore onClick={() => this.getPhotos()}>Show More...</ShowMore>
		];
	}
}

const ImageContainer = styled.div`
	grid-area: body;
	display: grid;
	grid-gap: 10px;
	grid-template-columns: repeat(auto-fit, minmax(${props => props.tileSize}, 1fr));
`;

const ShowMore = styled.div`
	font-size: 20px;
	color: ${stylesheet.blue};
	margin-top: 15px;
	text-align: center;
	cursor: pointer;
`;

const Message = styled.div`
	margin-top: 10px;
	padding: 10px;
	border: 1px solid rgba(34, 36, 38, 0.15);
	box-shadow: 0 1px 2px 0 rgba(34, 36, 38, 0.15);
	text-align: left;
	border-radius: 3px;
	width: 100%;
	height: ${props => (props.height ? props.height : '50px')};
	position: relative;
`;

const ActionContainer = styled.div`
	padding: 5px;
	display: inline-block;
	h4 {
		text-align: center;
		margin-bottom: 1px;
	}
`;

const Button = styled.button`
	background: ${props => (props.active ? 'rgba(91, 195, 235, 1)' : 'rgba(91, 195, 235, 0.4)')};
	border: 1px solid rgb(91, 195, 235);
	padding: 3px 8px;
	transition: 0.5s ease-in-out all;
	cursor: pointer;

	:hover {
		transition: 0.5s ease-in-out all;
		background: rgba(91, 195, 235, 0.6);
	}
	:focus {
		outline: none;
	}

	:first-child {
		border-right-width: 1px;
		border-top-left-radius: 3px;
		border-bottom-left-radius: 3px;
	}
	:nth-child(2) {
		border-right-width: 0px;
		border-left-width: 0px;
	}
	:last-child {
		border-top-right-radius: 3px;
		border-left-width: 1px;
		border-bottom-right-radius: 3px;
	}
`;
const mapStateToProps = state => ({
	routePrefix: state.routePrefix
});

export default connect(
	mapStateToProps,
	null
)(PhotoTab);
