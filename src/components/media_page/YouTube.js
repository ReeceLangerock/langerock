import React, { Component } from 'react';
import { connect } from 'react-redux';

import styled from 'styled-components';
import stylesheet from '../../stylesheet.js';
class YouTube extends Component {
	constructor(props) {
		super(props);
		this.state = {
			videos: []
		};
		this.renderYoutubeVideos = this.renderYoutubeVideos.bind(this);
	}
	async componentDidMount() {
		const res = await fetch(`${this.props.routePrefix}/media/youtube`);
		const data = await res.json();
		this.setState({
			videos: data
		});
	}

	renderYoutubeVideos() {
		const { videos } = this.state;
		console.log(this.state);
		if (!videos) {
			return <Message>Failed to retrieve YouTube links from server</Message>;
		}
		if (videos.length === 0) {
			return <Message>No YouTube videos added yet!</Message>;
		}
		return videos.map(video => {
			return (
				<VideoItem key={video.link}>
					<h1>{video.title}</h1>
					<iframe
						width="450"
						height="255"
						src={video.link}
						frameBorder="0"
						allow="autoplay; encrypted-media; picture-in-picture"
						allowFullScreen
					/>
				</VideoItem>
			);
		});
	}

	render() {
		return <VideoContainer>{this.renderYoutubeVideos()}</VideoContainer>;
	}
}

const Message = styled.div`
	padding: 14px;
	border: 1px solid rgba(34, 36, 38, 0.15);
	box-shadow: 0 1px 2px 0 rgba(34, 36, 38, 0.15);

	border-radius: 3px;
	width: 100%;
`;

const VideoItem = styled.div`
	margin-bottom: 10px;
	text-align: center;
	width: 49%;
	border-radius: 3px;
	:nth-child(odd) {
		margin-right: 10px;
	}
	padding: 5px;
	border: 1px solid rgba(34, 36, 38, 0.15);
	box-shadow:0 1px 2px 0 rgba(34,36,38,0.15) h1 {
		color: ${stylesheet.blue};
	}
`;

const VideoContainer = styled.div`
	width: 100%;
	display: flex;
	flex-wrap: wrap;
	justify-content: space-between;
`;

const mapStateToProps = state => ({
	routePrefix: state.routePrefix
});

export default connect(
	mapStateToProps,
	null
)(YouTube);
