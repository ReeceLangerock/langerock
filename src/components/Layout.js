import 'semantic-ui-css/semantic.min.css';

import styled from 'styled-components';
import { Container } from 'semantic-ui-react';
import Header from './Header';
import Footer from './Footer';
import stylesheet from './../stylesheet.js';

const Layout = props => (
	<PageWrapper>
		<Header />
		<PageContainer>
			<PageContent>{props.children}</PageContent>
			<div style={{ gridArea: 'sidebar' }} />
		</PageContainer>
		<Footer />
	</PageWrapper>
);

export default Layout;

const PageContainer = styled.div`
	max-width: 1000px;
	display: grid;
	width: 100%;
	grid-template-columns: 33.33% 33.33% 33.33%;

	grid-template-areas:
		'header header header'
		'body body body'
		'footer footer footer';
	margin: 0 auto;
	grid-row-gap: 15px;
	height: 100%;
	@media (max-width: 600px) {
		grid-template-areas:
			'header'
			'body'
			'sidebar'
			'footer';
		grid-template-columns: 100%;
	}
`;

const PageContent = styled.div`
	flex: 1;
	grid-area: body;
`;

const PageWrapper = styled.div`
	min-height: 100vh;
	display: flex;
	flex-direction: column;
`;
