import styled from 'styled-components';
import React from 'react';
import stylesheet from '../stylesheet';

export default class Message extends React.Component {
	displayMessage(message) {
		if (message.error) {
			this.setState({ messageType: 'error', message: message.error });
		}
		if (message.success) {
			this.setState({ messageType: 'success', message: 'Video successfully submitted' });
		}
	}

	render() {
		const { data } = this.props;
		if (data === undefined) {
			return null;
		}
		const error = data.error ? 'error' : 'success';
		const message = data.error ? data.error : data.success;
		return <StyledMessage color={error}>{message}</StyledMessage>;
	}
}
const StyledMessage = styled.div`
	color: ${props => (props.color === 'error' ? stylesheet.red : stylesheet.green)};
	font-size: 20px;
	margin-bottom: 10px;
`;
